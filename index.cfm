<html>
	<head>
		
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="span8">
	    			<h1>Welcome to DailyWeight.net</h1>
	    			<h3>An easy to use application to track how much you weigh so that you can visually see your weight loss progress.</h3>
	    			<h4>Sign up for an account today and make 2013 your skinniest year yet!</h4>
	    		</div>
				<div class="span4">
					<div class="well">
		 	 			<fieldset>
	    					<legend>Register</legend>
	    					<form action="register_success.cfm" method="POST">
								Email: <br>
								<input type="text" name="email"> <br>
								Password: <br>
								<input type="password" name="password"> <br>
								<button class="btn btn-success" name="register" type="submit">Register</button>
							</form>
	    			
	  					</fieldset>
					</div>
				</div>
			</div>
		</div>	
	</body>
</html>