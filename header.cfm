<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="container">
			<div class="navbar navbar-fixed-top">
				<div class="navbar-inner">
			    	<a class="brand" href="#">Daily Weight</a>
			    	<ul class="nav">
			      		<li><a href="index.cfm">Home</a></li>
			    	</ul>
					<ul class="nav pull-right">
					<cfif not isDefined("session.isLoggedIn")>
						<li><a href="register.cfm">Register</a></li>
						<li><a href="login.cfm">Log In</a></li>
					<cfelse>
						<li><a href="logout_success.cfm">Logout</a></li>
					</cfif>
				</ul>
			  	</div>
			</div>
  		</div>
	</body>
</html>