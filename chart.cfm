<html>
  <head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <cfoutput>
	<script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
		    var arrSize = '#arrayLen(session.weight)#'
	
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Date');
        data.addColumn('number', 'Weight');
 
		// Retrieve data from coldfusion and store in array
		var dailyWeights = <cfoutput>'#serializeJson(session.weight)#'</cfoutput>; 
    var dailyWeightsArr = jQuery.parseJSON(dailyWeights);

    var dw = <cfoutput>'#serializeJson(session.getData)#'</cfoutput>;
    var dwArr = jQuery.parseJSON(dw);
		var temp = dwArr.DATA.length;
		// Add each row in array to chart
		for(var i=0;i<dwArr.DATA.length;i++){
			data.addRow([dwArr.DATA[i][0],parseInt(dwArr.DATA[i][1])]);
		}
		
        // Set chart options
        var options = {'title':'Your Daily Weight',
                       'width':900,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
	</cfoutput>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
	<cfoutput>
		
	</cfoutput>
    <div id="chart_div"></div>
  </body>
</html>