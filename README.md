Daily Weight App
================

This is my very first web application written in ColdFusion. 

Daily Weight is a simple application that provides a private account for each user to track their weight daily. It also graphs each day and tells how far away you are from your goal weight. 

