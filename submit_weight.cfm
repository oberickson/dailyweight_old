<div class="container">
	<cfif not isDefined("session.isLoggedIn")>
		<h1>Not logged in!</h1>
		<br>
		<a href="login.cfm">Login</a>
		<cfabort>
	<cfelse>
		<!--- Get user id --->
		<cfquery name="getID">
			SELECT id FROM users WHERE email = ('#Session.email#')
		</cfquery>

		<cfset session.today = DateFormat(now(), "mm/dd/yyyy")>

		<!--- Insert new weight into database --->
		<cfif IsDefined('FORM.weight')>
			<cfquery name="insertWeight">
   				INSERT INTO daily(id, day, weight)
				VALUES(#getID.id#, '#session.today#', #FORM.weight#)
   			</cfquery>
		</cfif>
		<cfoutput>
		
		<div class="row">
			<div class="span4">
				<div class="well">
		 	 		<h3> Weight Submitted </h3>
		 	 		<a href="daily.cfm">Back to Profile</a>
				</div>
			</div>
		</div>
		</cfoutput>
		
		
	</cfif>
</div